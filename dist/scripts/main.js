(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"E:\\Doco-Skill\\front-end-boillerplate\\src\\src\\scripts\\main.js":[function(require,module,exports){
'use strict';

// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

// form-field-custom

$('.right-form').hide();

var formField = {
    init: function init() {
        this.dom();
        this.events();
    },
    dom: function dom() {
        this.$field = $('.form-field-custom');
        this.$label = $('label');

        // checkbox-dropshippers
        this.$checkDrop = $('#dropshippers');
    },
    events: function events() {
        var field = this.$field;
        var label = this.$label;
        var checkdrop = this.$checkDrop;

        field.on('focus', function () {
            var fieldAttr = $(this).attr('sn-field');

            $(fieldAttr).addClass('form-selected');
            $(this).addClass('green');
        });

        checkdrop.change(function () {
            if ($(this).is(":checked")) {
                $('.right-form').show();
            } else {
                $('.right-form').hide();
            }
        });
    }
};

formField.init();

},{}]},{},["E:\\Doco-Skill\\front-end-boillerplate\\src\\src\\scripts\\main.js"])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2NyaXB0cy9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLEVBQUEsYUFBQSxFQUFBLElBQUE7O0FBRUEsSUFBSSxZQUFZO0FBQ1osVUFBSyxTQUFBLElBQUEsR0FBVTtBQUNYLGFBQUEsR0FBQTtBQUNBLGFBQUEsTUFBQTtBQUhRLEtBQUE7QUFLWixTQUFLLFNBQUEsR0FBQSxHQUFVO0FBQ1gsYUFBQSxNQUFBLEdBQWMsRUFBZCxvQkFBYyxDQUFkO0FBQ0EsYUFBQSxNQUFBLEdBQWMsRUFBZCxPQUFjLENBQWQ7O0FBRUE7QUFDQSxhQUFBLFVBQUEsR0FBa0IsRUFBbEIsZUFBa0IsQ0FBbEI7QUFWUSxLQUFBO0FBYVosWUFBUSxTQUFBLE1BQUEsR0FBVTtBQUNkLFlBQUksUUFBUSxLQUFaLE1BQUE7QUFDQSxZQUFJLFFBQVEsS0FBWixNQUFBO0FBQ0EsWUFBSSxZQUFZLEtBQWhCLFVBQUE7O0FBRUEsY0FBQSxFQUFBLENBQUEsT0FBQSxFQUFrQixZQUFVO0FBQ3hCLGdCQUFJLFlBQVksRUFBQSxJQUFBLEVBQUEsSUFBQSxDQUFoQixVQUFnQixDQUFoQjs7QUFFQSxjQUFBLFNBQUEsRUFBQSxRQUFBLENBQUEsZUFBQTtBQUNBLGNBQUEsSUFBQSxFQUFBLFFBQUEsQ0FBQSxPQUFBO0FBSkosU0FBQTs7QUFPQSxrQkFBQSxNQUFBLENBQWlCLFlBQVU7QUFDdkIsZ0JBQUksRUFBQSxJQUFBLEVBQUEsRUFBQSxDQUFKLFVBQUksQ0FBSixFQUE0QjtBQUN4QixrQkFBQSxhQUFBLEVBQUEsSUFBQTtBQURKLGFBQUEsTUFFSztBQUNELGtCQUFBLGFBQUEsRUFBQSxJQUFBO0FBQ0g7QUFMTCxTQUFBO0FBT0g7QUFoQ1csQ0FBaEI7O0FBbUNBLFVBQUEsSUFBQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7ZnVuY3Rpb24gcihlLG4sdCl7ZnVuY3Rpb24gbyhpLGYpe2lmKCFuW2ldKXtpZighZVtpXSl7dmFyIGM9XCJmdW5jdGlvblwiPT10eXBlb2YgcmVxdWlyZSYmcmVxdWlyZTtpZighZiYmYylyZXR1cm4gYyhpLCEwKTtpZih1KXJldHVybiB1KGksITApO3ZhciBhPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIraStcIidcIik7dGhyb3cgYS5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGF9dmFyIHA9bltpXT17ZXhwb3J0czp7fX07ZVtpXVswXS5jYWxsKHAuZXhwb3J0cyxmdW5jdGlvbihyKXt2YXIgbj1lW2ldWzFdW3JdO3JldHVybiBvKG58fHIpfSxwLHAuZXhwb3J0cyxyLGUsbix0KX1yZXR1cm4gbltpXS5leHBvcnRzfWZvcih2YXIgdT1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlLGk9MDtpPHQubGVuZ3RoO2krKylvKHRbaV0pO3JldHVybiBvfXJldHVybiByfSkoKSIsIi8vIGltcG9ydCBNdWMgZnJvbSAndmlld3MvbXVjJ1xyXG4vLyBpbXBvcnQgVmlldyBmcm9tICd2aWV3cy92aWV3J1xyXG4vLyBpbXBvcnQgQ29tcG9uZW50IGZyb20gJ2NvbXBvbmVudHMvdGFnaWhhbi5jb21wb25lbnQnXHJcbi8vIGltcG9ydCBNaXRyYSBmcm9tICdjb21wb25lbnRzL21pdHJhLmNvbXBvbmVudCdcclxuLy8gaW1wb3J0IFBvcHVwIGZyb20gJ2NvbXBvbmVudHMvcG9wdXAuY29tcG9uZW50J1xyXG5cclxuLy8gZm9ybS1maWVsZC1jdXN0b21cclxuXHJcbiQoJy5yaWdodC1mb3JtJykuaGlkZSgpXHJcblxyXG52YXIgZm9ybUZpZWxkID0ge1xyXG4gICAgaW5pdDpmdW5jdGlvbigpe1xyXG4gICAgICAgIHRoaXMuZG9tKCk7XHJcbiAgICAgICAgdGhpcy5ldmVudHMoKTtcclxuICAgIH0sXHJcbiAgICBkb206IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdGhpcy4kZmllbGQgPSAkKCcuZm9ybS1maWVsZC1jdXN0b20nKVxyXG4gICAgICAgIHRoaXMuJGxhYmVsID0gJCgnbGFiZWwnKVxyXG5cclxuICAgICAgICAvLyBjaGVja2JveC1kcm9wc2hpcHBlcnNcclxuICAgICAgICB0aGlzLiRjaGVja0Ryb3AgPSAkKCcjZHJvcHNoaXBwZXJzJylcclxuXHJcbiAgICB9LFxyXG4gICAgZXZlbnRzOiBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciBmaWVsZCA9IHRoaXMuJGZpZWxkO1xyXG4gICAgICAgIHZhciBsYWJlbCA9IHRoaXMuJGxhYmVsXHJcbiAgICAgICAgdmFyIGNoZWNrZHJvcCA9IHRoaXMuJGNoZWNrRHJvcFxyXG5cclxuICAgICAgICBmaWVsZC5vbignZm9jdXMnLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICB2YXIgZmllbGRBdHRyID0gJCh0aGlzKS5hdHRyKCdzbi1maWVsZCcpXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAkKGZpZWxkQXR0cikuYWRkQ2xhc3MoJ2Zvcm0tc2VsZWN0ZWQnKVxyXG4gICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdncmVlbicpXHJcbiAgICAgICAgfSlcclxuXHJcbiAgICAgICAgY2hlY2tkcm9wLmNoYW5nZShmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICBpZiAoJCh0aGlzKS5pcyhcIjpjaGVja2VkXCIpKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcucmlnaHQtZm9ybScpLnNob3coKVxyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICQoJy5yaWdodC1mb3JtJykuaGlkZSgpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgfVxyXG59XHJcblxyXG5mb3JtRmllbGQuaW5pdCgpO1xyXG4iXX0=
