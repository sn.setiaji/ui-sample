// import Muc from 'views/muc'
// import View from 'views/view'
// import Component from 'components/tagihan.component'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

// form-field-custom

$('.right-form').hide()

var formField = {
    init:function(){
        this.dom();
        this.events();
    },
    dom: function(){
        this.$field = $('.form-field-custom')
        this.$label = $('label')

        // checkbox-dropshippers
        this.$checkDrop = $('#dropshippers')

    },
    events: function(){
        var field = this.$field;
        var label = this.$label
        var checkdrop = this.$checkDrop

        field.on('focus', function(){
            var fieldAttr = $(this).attr('sn-field')
            
            $(fieldAttr).addClass('form-selected')
            $(this).addClass('green')
        })

        checkdrop.change(function(){
            if ($(this).is(":checked")) {
                $('.right-form').show()
            }else{
                $('.right-form').hide()
            }
        })
    }
}

formField.init();
